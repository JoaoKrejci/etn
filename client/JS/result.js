import { Meteor } from 'meteor/meteor';
import { Person } from '../../collections/person.js';

Template.resultado.helpers({
  achados: () => {
    Meteor.subscribe(SHA256(Session.get('imageData')), {
      onError: () => {
        alert("Ocorreu um erro ao tentar obter o resultado do servidor:\n" +
        "Verifique a conexão de internet e tente novamente mais tarde");
        BlazeLayout.render('home', {main: 'list'});
      },
    });
    return Person.find().fetch();
  }
});
