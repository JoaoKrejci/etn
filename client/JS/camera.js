import { Session } from 'meteor/session'

function cameraOptions(source) {
  this.quality = 50;
  this.destinationType = 0;
  this.sourceType = source;
  this.encodingType = 0;
  this.allowEdit = false;
};

Template.camera.events({
  "click #captura": (e, t) => {
    navigator.camera.getPicture(
      (imageData) => {            //On Sucesso
        submitImage(imageData);
      },
      (message) => {             //On Fracasso
        alert('Erro: ' + message);
      },
      new cameraOptions(1) //Camera
    )
    e.preventDefault();
  },

  "click #load": function(e, t) {
    navigator.camera.getPicture(
      (imageData) => {            //On Sucesso
        submitImage(imageData);
      },
      (message) => {             //On Fracasso
        alert('Erro: ' + message);
      },
      new cameraOptions(0) //Galeria
    )
    e.preventDefault();
  }
});

function submitImage(imageData){
  $('#pic').attr('src', "data:image/jpeg;base64, " + imageData);

  Meteor.call("process", imageData, (err) => {
    if(err) {
      alert('Um erro aconteceu. Tente novamente mais tarde.');
      return;
    }
    Session.set('imageData', imageData);
    BlazeLayout.render('home', {main: 'resultado'});
  }
);
}
