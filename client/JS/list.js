import { Person } from '../../collections/person.js';

Template.list.helpers({
    person: () => {
        Meteor.subscribe('person');
        return Person.find().fetch();
    }
});

Template.list.events({
    "click .found": function(event, template){
        BlazeLayout.render('home' , {main: 'camera'});
    }
});
