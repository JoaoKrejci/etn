FlowRouter.route("/", {
      name:"Home List",
      action () {
          BlazeLayout.render('home', {main: 'list'});
      }
});

FlowRouter.route("/camera", {
      name: "Camera",
      action () {
          BlazeLayout.render('home', {main: 'camera'});
      }
});

FlowRouter.route("/settings", {
    name:"Configurações",
    action () {
        BlazeLayout.render('home', {main: 'settings'});
    }
});

FlowRouter.route("/info", {
    name:"Informações do Aplicativo",
    action () {
        BlazeLayout.render('home', {main: 'info'});
    }
});
