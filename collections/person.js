import { Mongo } from 'meteor/mongo';

export const Person = new Mongo.Collection("person");

if (Meteor.isServer){
  Meteor.publish('person', ()=> {
    return Person.find();
  });
}
