#include <openbr/openbr_plugin.h>
#include <iostream>
using namespace std;
using namespace br;

int main(int argc, char *argv[]){
	Context::initialize(argc, argv,"", false);

	//Declaração de variaveis
		Template pessoa(argv[1]);
		QList<float> comparacoes;
		TemplateList pessoas = TemplateList::fromGallery("/home/joao/Projects/ETN/server/Desaparecidos.xml");
		QSharedPointer<Transform> transform = Transform::fromAlgorithm("FaceRecognition");
		QSharedPointer<Distance> compare = Distance::fromAlgorithm("FaceRecognition");
	//Declaração de variaveis

	Globals->enrollAll = true;
	pessoas >> *transform;
	Globals->enrollAll = false;
	pessoa  >> *transform;
	comparacoes = compare->compare(pessoas, pessoa);

	for(int i = 0; i < comparacoes.length(); i++){
		if(comparacoes.at(i) > 0.17){
			cout << pessoas.at(i).file.name.toStdString() << endl;
		}
	}

	Context::finalize();
	return 0;
}
