import { Meteor } from 'meteor/meteor';
import { Person } from '../collections/person.js';

const
home = process.env.PWD.replace('.meteor/local/cordova-build', ''),
fs      = require('fs'),
exec    = require('child_process'),
path    =`${home}server/pictures/`,
etnbr   = path.replace('pictures/', 'ETNBr/etnbr ')
;

Meteor.startup(() => {
  Meteor.methods({
    process: (data) => {
      var img = `data:image/jpg;base64,${data}`,
      buf = new Buffer(img.replace(/^data:image\/\w+;base64,/, ""), 'base64');
      const pic = `${path}${SHA256(data)}.jpg`;

      fs.writeFileSync(pic, buf);
      delete buf, img;

      var result = exec.execSync(etnbr + pic).toString().split("\n");
      exec.exec("rm " + pic);

      console.log(SHA256(data));

      Meteor.publish(SHA256(data), () => {
        return Person.find({caminhoAbsoluto: {$in: result }});
      });
    },
    stopPublish: (hash) => {
      Meteor.publish(hash, this.stop());
    },
  });
});
