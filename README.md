# README #

Dentro deste repositório estão todos os arquivos do projeto ETN: Cliente e Servidor.

### Cliente ###
O cliente ETN se trata de um aplicativo híbrido para smartphone elaborado nos padrões de desenvolvimento web e construido com a framework meteor.
#### Utilização ####
Página principal contém lista de pessoas desaparecidas da região. O botão menu disponibiliza acesso às funcionalidades: câmera, galeria, configurações e app info/credits.

 - list: apresenta pessoas cadastradas no banco e seus dados pessoais. Exibe também um botão "Encontrei" que permite fotografar(câmera) ou selecionar uma foto(galeria);

 - câmera: função nativa que toma a tela apresentando, em imagem, o fluxo recebido da câmera;

 - galeria: permite escolher um arquivo de imagem desejado;

 - configurações: área para que o usuário configure uma conta de e-mail para agilizar a notificação direta dos órgãos responsáveis em caso de desaparecimento

 - app info: informações de licença, créditos e criadores

### Servidor ###
Até o presente momento o servidor possui três modulos individuais e cooperativos:

#### etnbr ####
O _etnbr_ é aplicativo que é chamado pelo processo corrente do meteor no servidor, ele tem a responsabilidade de receber um caminho de imagem, comparar com as presentes no arquivo _Desaparecidos.xml_ e retornar os caminhos de imagens que correspondem em semelhança com a inserida.

#### etn.enroll ####
O _etn.enroll_ é um arquivo em shell script que, por meio do aplicativo br, realiza o enroll das imagens presentes em public/Fotos e grava no arquivo _Desaparecidos.xml_.
_Desaparecidos.xml_ armazena as informações das fotos processadas pela _openbr_ de forma que elas possam ser lidas por aplicações que usem a _openbr_ de forma mais rápida por ja conterem os dados do processamento de face.

#### etn.record ####
O etn.record tem a intenção de agilizar a forma de registro de pessoas desaparecidas no banco de dados do ETN por meio de uma conexão TCP e o upload das imagens por meio de FTP. (incopleto)

### Instalação da OpenBr ###
Dependencias:

 - OpenCv >= 2.4.11 < 3.0
 - Qt == 5.4.*

Link para download da Qt: http://download.qt.io/archive/qt/

** A biblioteca Qt em si é enorme, o desejável é instalar o pacote Qt-Core ou Qt-Base para evitar ocupar espaço desnecessário em disco.

Durante a compilação é possível que ele aponte um problema com uma variável do http_parser do Node.js. O problema é o nome da variável que com a versão atual mudou. 
Este problema pode ser facilmente resolvido mudando o nome da variável em um dos arquivos do openbr(não me recordo qual, mas o make te diz).


### Técnologias Utilizadas ###
 - Meteor (HTML,CSS,JS,...)
 - OpenBR (C++)
 - MongoDB
 - NodeJS

### Observações ###
 - Construido e testado em ambiente Linux sem visão de portagem no horizonte. (Se quiser tentar fique a vontade)
 - Tenho a intenção de montar um script de preparação de dependencia em breve. (A instalação da OpenBr é realmente MUITO chata)
 - A proposta é que o ETN funcione em qualquer celular Android ou iOS devidamente atualizados, porém devido a falta de recursos o que podemos garantir até o presente momento é que ele funciona bem com o Android 5.0 e 5.1.
