var require = meteorInstall({"client":{"template.body.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.body.js                                                                                  //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("home");                                                                               // 2
Template["home"] = new Template("Template.home", (function() {                                              // 3
  var view = this;                                                                                          // 4
  return [ HTML.HEADER("\n\t\t", HTML.A({                                                                   // 5
    href: "#",                                                                                              // 6
    class: "exibe"                                                                                          // 7
  }, HTML.SVG({                                                                                             // 8
    id: "menu"                                                                                              // 9
  }, "\n\t\t\t", HTML.USE({                                                                                 // 10
    "xlink:href": "/icons.svg#menu"                                                                         // 11
  }), "\n\t\t")), "\n\t\t", HTML.Raw('<div class="logo-place">\n\t\t\t<img id="logo" src="imgs/ETNSmall.png" alt="">\n\t\t</div>'), "\n\t\t", HTML.NAV({
    class: "dispose"                                                                                        // 13
  }, "\n\t\t\t\t", Spacebars.include(view.lookupTemplate("buttons")), "\n\t\t"), "\n\t"), "\n\t", HTML.MAIN("\n\t\t", Blaze._TemplateWith(function() {
    return {                                                                                                // 15
      template: Spacebars.call(view.lookup("main"))                                                         // 16
    };                                                                                                      // 17
  }, function() {                                                                                           // 18
    return Spacebars.include(function() {                                                                   // 19
      return Spacebars.call(Template.__dynamic);                                                            // 20
    });                                                                                                     // 21
  }), "\n\t") ];                                                                                            // 22
}));                                                                                                        // 23
                                                                                                            // 24
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.buttons.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.buttons.js                                                                               //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("buttons");                                                                            // 2
Template["buttons"] = new Template("Template.buttons", (function() {                                        // 3
  var view = this;                                                                                          // 4
  return [ HTML.DIV({                                                                                       // 5
    class: "centralize btn"                                                                                 // 6
  }, "\n        ", HTML.A({                                                                                 // 7
    href: "/"                                                                                               // 8
  }, "\n            ", HTML.SVG({                                                                           // 9
    id: "list"                                                                                              // 10
  }, "\n                ", HTML.USE({                                                                       // 11
    "xlink:href": "/icons.svg#list"                                                                         // 12
  }), "\n            "), "\n        "), "\n    "), "\n    ", HTML.DIV({                                     // 13
    class: "centralize btn"                                                                                 // 14
  }, "\n        ", HTML.A({                                                                                 // 15
    href: "/camera"                                                                                         // 16
  }, "\n            ", HTML.SVG({                                                                           // 17
    id: "camera"                                                                                            // 18
  }, "\n                ", HTML.USE({                                                                       // 19
    "xlink:href": "/icons.svg#camera"                                                                       // 20
  }), "\n            "), "\n        "), "\n    "), "\n    ", HTML.DIV({                                     // 21
    class: "centralize btn"                                                                                 // 22
  }, "\n        ", HTML.A({                                                                                 // 23
    href: "/settings"                                                                                       // 24
  }, "\n            ", HTML.SVG({                                                                           // 25
    id: "settings"                                                                                          // 26
  }, "\n                ", HTML.USE({                                                                       // 27
    "xlink:href": "/icons.svg#settings"                                                                     // 28
  }), "\n            "), "\n        "), "\n    "), "\n    ", HTML.DIV({                                     // 29
    class: "centralize btn"                                                                                 // 30
  }, "\n        ", HTML.A({                                                                                 // 31
    href: "/info"                                                                                           // 32
  }, "\n            ", HTML.SVG({                                                                           // 33
    id: "info"                                                                                              // 34
  }, "\n                ", HTML.USE({                                                                       // 35
    "xlink:href": "/icons.svg#info"                                                                         // 36
  }), "\n            "), "\n        "), "\n    ") ];                                                        // 37
}));                                                                                                        // 38
                                                                                                            // 39
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.camera.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.camera.js                                                                                //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("camera");                                                                             // 2
Template["camera"] = new Template("Template.camera", (function() {                                          // 3
  var view = this;                                                                                          // 4
  return [ HTML.Raw('<div id="foto">\n      <img src="" alt="" id="pic">\n    </div>\n    '), HTML.DIV({    // 5
    class: "camera"                                                                                         // 6
  }, "\n        ", HTML.DIV({                                                                               // 7
    class: "icns"                                                                                           // 8
  }, "\n              ", HTML.SVG({                                                                         // 9
    id: "captura"                                                                                           // 10
  }, "\n                  ", HTML.USE({                                                                     // 11
    "xlink:href": "/icons.svg#camera"                                                                       // 12
  }), "\n              "), "\n        "), "\n        ", HTML.DIV({                                          // 13
    class: "icns"                                                                                           // 14
  }, "\n              ", HTML.SVG({                                                                         // 15
    id: "load"                                                                                              // 16
  }, "\n                  ", HTML.USE({                                                                     // 17
    "xlink:href": "/icons.svg#load"                                                                         // 18
  }), "\n              "), "\n        "), "\n    ") ];                                                      // 19
}));                                                                                                        // 20
                                                                                                            // 21
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.info.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.info.js                                                                                  //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("info");                                                                               // 2
Template["info"] = new Template("Template.info", (function() {                                              // 3
  var view = this;                                                                                          // 4
  return HTML.Raw('<section class="dataLabel centralize info">\n        <div id="infos">\n            <h5>ETN - Exploradores da Terra do Nunca</h5>\n            <br>\n            <p>Desenvolvido por:\n                <br>\n                <ul id="authors">\n                    <li><span>Adler Mesquita Orteney</span></li>\n                    <li><span>João Pedro de Albuquerque Krejci</span></li>\n                </ul>\n            </p>\n            <br>\n            <p>Desenvolvimento com <em>Meteor</em></p>\n            <br>\n            <img src="/imgs/ETN-Completo.png" alt="ETN" width="70%">\n        </div>\n    </section>');
}));                                                                                                        // 6
                                                                                                            // 7
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.list.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.list.js                                                                                  //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("list");                                                                               // 2
Template["list"] = new Template("Template.list", (function() {                                              // 3
  var view = this;                                                                                          // 4
  return HTML.DIV({                                                                                         // 5
    class: "lista"                                                                                          // 6
  }, "\n\n    ", Blaze.Each(function() {                                                                    // 7
    return Spacebars.call(view.lookup("person"));                                                           // 8
  }, function() {                                                                                           // 9
    return [ "\n      ", Spacebars.include(view.lookupTemplate("pessoa")), "\n    " ];                      // 10
  }), HTML.Raw("\n\n  <footer>Exploradores da Terra do Nunca</footer>\n  "));                               // 11
}));                                                                                                        // 12
                                                                                                            // 13
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.pessoa.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.pessoa.js                                                                                //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("pessoa");                                                                             // 2
Template["pessoa"] = new Template("Template.pessoa", (function() {                                          // 3
  var view = this;                                                                                          // 4
  return HTML.DIV({                                                                                         // 5
    class: "card"                                                                                           // 6
  }, "\n      ", HTML.DIV({                                                                                 // 7
    class: "image"                                                                                          // 8
  }, "\n          ", HTML.IMG({                                                                             // 9
    src: function() {                                                                                       // 10
      return Spacebars.mustache(view.lookup("caminhoMeteor"));                                              // 11
    },                                                                                                      // 12
    alt: function() {                                                                                       // 13
      return Spacebars.mustache(view.lookup("nome"));                                                       // 14
    },                                                                                                      // 15
    class: "picture"                                                                                        // 16
  }), "\n      "), "\n      ", HTML.DIV({                                                                   // 17
    class: "dataLabel"                                                                                      // 18
  }, "\n          ", HTML.P(HTML.Raw("<span>Nome:</span>"), " ", Blaze.View("lookup:nome", function() {     // 19
    return Spacebars.mustache(view.lookup("nome"));                                                         // 20
  })), "\n          ", HTML.P(HTML.Raw("<span>Idade:</span>"), " ", Blaze.View("lookup:idade", function() {
    return Spacebars.mustache(view.lookup("idade"));                                                        // 22
  })), "\n          ", HTML.P(HTML.Raw("<span>Sexo:</span>"), " ", Blaze.View("lookup:sexo", function() {   // 23
    return Spacebars.mustache(view.lookup("sexo"));                                                         // 24
  })), "\n          ", HTML.Raw("<!-- Isso aqui não é pra aparecer no Result -->"), "\n          ", HTML.Raw('<button class="found">Encontrei</button>'), "\n          ", HTML.Raw("<!-- Isso aqui não é pra aparecer no Result -->"), "\n          ", HTML.P(HTML.Raw("<span>Local:</span>"), " ", Blaze.View("lookup:cidade", function() {
    return Spacebars.mustache(view.lookup("cidade"));                                                       // 26
  })), "\n          ", HTML.P(HTML.Raw("<span>Data:</span>"), " ", Blaze.View("lookup:dataDesaparecimento", function() {
    return Spacebars.mustache(view.lookup("dataDesaparecimento"));                                          // 28
  })), "\n      "), "\n  ");                                                                                // 29
}));                                                                                                        // 30
                                                                                                            // 31
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.resultado.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.resultado.js                                                                             //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("resultado");                                                                          // 2
Template["resultado"] = new Template("Template.resultado", (function() {                                    // 3
  var view = this;                                                                                          // 4
  return HTML.DIV({                                                                                         // 5
    class: "lista result"                                                                                   // 6
  }, "\n\n    ", Blaze.Each(function() {                                                                    // 7
    return Spacebars.call(view.lookup("achados"));                                                          // 8
  }, function() {                                                                                           // 9
    return [ "\n      ", Spacebars.include(view.lookupTemplate("pessoa")), "\n    " ];                      // 10
  }), HTML.Raw("\n\n    <footer><p>Exploradores da Terra do Nunca</p></footer>\n  "));                      // 11
}));                                                                                                        // 12
                                                                                                            // 13
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.settings.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/template.settings.js                                                                              //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
                                                                                                            // 1
Template.__checkName("settings");                                                                           // 2
Template["settings"] = new Template("Template.settings", (function() {                                      // 3
  var view = this;                                                                                          // 4
  return HTML.SECTION({                                                                                     // 5
    class: "dataLabel login"                                                                                // 6
  }, HTML.Raw('\n        <p class="nota">Configure sua conta de email no aplicativo para contatar\n          as entidades locais responsáveis por pessoas desparecidas</p>\n        '), HTML.FORM("\n            ", HTML.Raw('<input type="text" class="mail" placeholder="Email">'), "\n            ", HTML.Raw('<input type="password" class="mail" placeholder="Senha">'), "\n            ", HTML.DIV({
    class: "buttonSubmit"                                                                                   // 8
  }, "\n                ", HTML.BUTTON({                                                                    // 9
    type: "submit",                                                                                         // 10
    class: "button"                                                                                         // 11
  }, "\n                    ", HTML.SVG({                                                                   // 12
    id: "yes"                                                                                               // 13
  }, "\n                        ", HTML.USE({                                                               // 14
    "xlink:href": "/icons.svg#yes"                                                                          // 15
  }), "\n                    "), "\n                "), "\n            "), "\n        "), HTML.Raw('\n        <section name="Telefones" class="phones dataLabel">\n            <ul>\n                <h4>Telefones dos Órgãos</h4>\n                <br>\n                <li>Secretária de Segurança Pública:  +99 99 99999 9999</li>\n                <li>Departamento de Pessoas Desaparecidas:  +99 99 99999 9999</li>\n                <li>Departamento de Polícia:  190</li>\n            </ul>\n        </section>\n    '));
}));                                                                                                        // 17
                                                                                                            // 18
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"JS":{"camera.js":["meteor/session",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/JS/camera.js                                                                                      //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var Session;module.import('meteor/session',{"Session":function(v){Session=v}});                             // 1
                                                                                                            //
function cameraOptions(source) {                                                                            // 3
  this.quality = 50;                                                                                        // 4
  this.destinationType = 0;                                                                                 // 5
  this.sourceType = source;                                                                                 // 6
  this.encodingType = 0;                                                                                    // 7
  this.allowEdit = false;                                                                                   // 8
};                                                                                                          // 9
                                                                                                            //
Template.camera.events({                                                                                    // 11
  "click #captura": function () {                                                                           // 12
    function clickCaptura(e, t) {                                                                           // 12
      navigator.camera.getPicture(function (imageData) {                                                    // 13
        //On Sucesso                                                                                        //
        submitImage(imageData);                                                                             // 15
      }, function (message) {                                                                               // 16
        //On Fracasso                                                                                       //
        alert('Erro: ' + message);                                                                          // 18
      }, new cameraOptions(1) //Camera                                                                      // 19
      );                                                                                                    // 13
      e.preventDefault();                                                                                   // 22
    }                                                                                                       // 23
                                                                                                            //
    return clickCaptura;                                                                                    // 12
  }(),                                                                                                      // 12
                                                                                                            //
  "click #load": function () {                                                                              // 25
    function clickLoad(e, t) {                                                                              // 25
      navigator.camera.getPicture(function (imageData) {                                                    // 26
        //On Sucesso                                                                                        //
        submitImage(imageData);                                                                             // 28
      }, function (message) {                                                                               // 29
        //On Fracasso                                                                                       //
        alert('Erro: ' + message);                                                                          // 31
      }, new cameraOptions(0) //Galeria                                                                     // 32
      );                                                                                                    // 26
      e.preventDefault();                                                                                   // 35
    }                                                                                                       // 36
                                                                                                            //
    return clickLoad;                                                                                       // 25
  }()                                                                                                       // 25
});                                                                                                         // 11
                                                                                                            //
function submitImage(imageData) {                                                                           // 39
  $('#pic').attr('src', "data:image/jpeg;base64, " + imageData);                                            // 40
                                                                                                            //
  Meteor.call("process", imageData, function (err) {                                                        // 42
    if (err) {                                                                                              // 43
      alert('Um erro aconteceu. Tente novamente mais tarde.');                                              // 44
      return;                                                                                               // 45
    }                                                                                                       // 46
    Session.set('imageData', imageData);                                                                    // 47
    BlazeLayout.render('home', { main: 'resultado' });                                                      // 48
  });                                                                                                       // 49
}                                                                                                           // 51
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"list.js":["../../collections/person.js",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/JS/list.js                                                                                        //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var Person;module.import('../../collections/person.js',{"Person":function(v){Person=v}});                   // 1
                                                                                                            //
Template.list.helpers({                                                                                     // 3
    person: function () {                                                                                   // 4
        function person() {                                                                                 // 4
            Meteor.subscribe('person');                                                                     // 5
            return Person.find().fetch();                                                                   // 6
        }                                                                                                   // 7
                                                                                                            //
        return person;                                                                                      // 4
    }()                                                                                                     // 4
});                                                                                                         // 3
                                                                                                            //
Template.list.events({                                                                                      // 10
    "click .found": function () {                                                                           // 11
        function clickFound(event, template) {                                                              // 11
            BlazeLayout.render('home', { main: 'camera' });                                                 // 12
        }                                                                                                   // 13
                                                                                                            //
        return clickFound;                                                                                  // 11
    }()                                                                                                     // 11
});                                                                                                         // 10
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"pessoa.js":["meteor/meteor","../../collections/person.js",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/JS/pessoa.js                                                                                      //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var Meteor;module.import('meteor/meteor',{"Meteor":function(v){Meteor=v}});var Person;module.import('../../collections/person.js',{"Person":function(v){Person=v}});
                                                                                                            // 2
                                                                                                            //
Template.pessoa.events({                                                                                    // 4
  "click .card": function () {                                                                              // 5
    function clickCard() {                                                                                  // 5
      alert("Quero que isso aqui apareça só no Result");                                                    // 6
    }                                                                                                       // 7
                                                                                                            //
    return clickCard;                                                                                       // 5
  }()                                                                                                       // 5
});                                                                                                         // 4
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"result.js":["meteor/meteor","../../collections/person.js",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/JS/result.js                                                                                      //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var Meteor;module.import('meteor/meteor',{"Meteor":function(v){Meteor=v}});var Person;module.import('../../collections/person.js',{"Person":function(v){Person=v}});
                                                                                                            // 2
                                                                                                            //
Template.resultado.helpers({                                                                                // 4
  achados: function () {                                                                                    // 5
    function achados() {                                                                                    // 5
      Meteor.subscribe(SHA256(Session.get('imageData')), {                                                  // 6
        onError: function () {                                                                              // 7
          function onError() {                                                                              // 7
            alert("Ocorreu um erro ao tentar obter o resultado do servidor:\n" + "Verifique a conexão de internet e tente novamente mais tarde");
            BlazeLayout.render('home', { main: 'list' });                                                   // 10
          }                                                                                                 // 11
                                                                                                            //
          return onError;                                                                                   // 7
        }()                                                                                                 // 7
      });                                                                                                   // 6
      return Person.find().fetch();                                                                         // 13
    }                                                                                                       // 14
                                                                                                            //
    return achados;                                                                                         // 5
  }()                                                                                                       // 5
});                                                                                                         // 4
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"main.js":["meteor/templating","meteor/reactive-var",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// client/JS/main.js                                                                                        //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
var Template;module.import('meteor/templating',{"Template":function(v){Template=v}});var ReactiveVar;module.import('meteor/reactive-var',{"ReactiveVar":function(v){ReactiveVar=v}});
                                                                                                            // 2
                                                                                                            //
Template.home.events({                                                                                      // 4
    "click .exibe": function () {                                                                           // 5
        function clickExibe(event, tpl) {                                                                   // 5
            $('nav').slideToggle('slow');                                                                   // 6
        }                                                                                                   // 7
                                                                                                            //
        return clickExibe;                                                                                  // 5
    }()                                                                                                     // 5
});                                                                                                         // 4
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}},"lib":{"router.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// lib/router.js                                                                                            //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
FlowRouter.route("/", {                                                                                     // 1
    name: "Home List",                                                                                      // 2
    action: function () {                                                                                   // 3
        function action() {                                                                                 // 1
            BlazeLayout.render('home', { main: 'list' });                                                   // 4
        }                                                                                                   // 5
                                                                                                            //
        return action;                                                                                      // 1
    }()                                                                                                     // 1
});                                                                                                         // 1
                                                                                                            //
FlowRouter.route("/camera", {                                                                               // 8
    name: "Camera",                                                                                         // 9
    action: function () {                                                                                   // 10
        function action() {                                                                                 // 8
            BlazeLayout.render('home', { main: 'camera' });                                                 // 11
        }                                                                                                   // 12
                                                                                                            //
        return action;                                                                                      // 8
    }()                                                                                                     // 8
});                                                                                                         // 8
                                                                                                            //
FlowRouter.route("/settings", {                                                                             // 15
    name: "Configurações",                                                                                  // 16
    action: function () {                                                                                   // 17
        function action() {                                                                                 // 15
            BlazeLayout.render('home', { main: 'settings' });                                               // 18
        }                                                                                                   // 19
                                                                                                            //
        return action;                                                                                      // 15
    }()                                                                                                     // 15
});                                                                                                         // 15
                                                                                                            //
FlowRouter.route("/info", {                                                                                 // 22
    name: "Informações do Aplicativo",                                                                      // 23
    action: function () {                                                                                   // 24
        function action() {                                                                                 // 22
            BlazeLayout.render('home', { main: 'info' });                                                   // 25
        }                                                                                                   // 26
                                                                                                            //
        return action;                                                                                      // 22
    }()                                                                                                     // 22
});                                                                                                         // 22
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"collections":{"person.js":["meteor/mongo",function(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                          //
// collections/person.js                                                                                    //
//                                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                            //
module.export({Person:function(){return Person}});var Mongo;module.import('meteor/mongo',{"Mongo":function(v){Mongo=v}});
                                                                                                            //
var Person = new Mongo.Collection("person");                                                                // 3
                                                                                                            //
if (Meteor.isServer) {                                                                                      // 5
  Meteor.publish('person', function () {                                                                    // 6
    return Person.find();                                                                                   // 7
  });                                                                                                       // 8
}                                                                                                           // 9
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json",".html",".css"]});
require("./client/template.body.js");
require("./client/template.buttons.js");
require("./client/template.camera.js");
require("./client/template.info.js");
require("./client/template.list.js");
require("./client/template.pessoa.js");
require("./client/template.resultado.js");
require("./client/template.settings.js");
require("./lib/router.js");
require("./client/JS/camera.js");
require("./client/JS/list.js");
require("./client/JS/pessoa.js");
require("./client/JS/result.js");
require("./collections/person.js");
require("./client/JS/main.js");