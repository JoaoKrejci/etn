var require = meteorInstall({"lib":{"router.js":function(){

/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
// lib/router.js                                                                   //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////
                                                                                   //
FlowRouter.route("/", {                                                            // 1
    name: "Home List",                                                             // 2
    action: function action() {                                                    // 3
        BlazeLayout.render('home', { main: 'list' });                              // 4
    }                                                                              // 5
});                                                                                // 1
                                                                                   //
FlowRouter.route("/camera", {                                                      // 8
    name: "Camera",                                                                // 9
    action: function action() {                                                    // 10
        BlazeLayout.render('home', { main: 'camera' });                            // 11
    }                                                                              // 12
});                                                                                // 8
                                                                                   //
FlowRouter.route("/settings", {                                                    // 15
    name: "Configurações",                                                         // 16
    action: function action() {                                                    // 17
        BlazeLayout.render('home', { main: 'settings' });                          // 18
    }                                                                              // 19
});                                                                                // 15
                                                                                   //
FlowRouter.route("/info", {                                                        // 22
    name: "Informações do Aplicativo",                                             // 23
    action: function action() {                                                    // 24
        BlazeLayout.render('home', { main: 'info' });                              // 25
    }                                                                              // 26
});                                                                                // 22
/////////////////////////////////////////////////////////////////////////////////////

}},"collections":{"person.js":["meteor/mongo",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
// collections/person.js                                                           //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////
                                                                                   //
module.export({Person:function(){return Person}});var Mongo;module.import('meteor/mongo',{"Mongo":function(v){Mongo=v}});
                                                                                   //
var Person = new Mongo.Collection("person");                                       // 3
                                                                                   //
if (Meteor.isServer) {                                                             // 5
  Meteor.publish('person', function () {                                           // 6
    return Person.find();                                                          // 7
  });                                                                              // 8
}                                                                                  // 9
/////////////////////////////////////////////////////////////////////////////////////

}]},"server":{"main.js":["meteor/meteor","../collections/person.js","fs","child_process",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
// server/main.js                                                                  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////
                                                                                   //
var Meteor;module.import('meteor/meteor',{"Meteor":function(v){Meteor=v}});var Person;module.import('../collections/person.js',{"Person":function(v){Person=v}});var _this = this;
                                                                                   //
                                                                                   // 1
                                                                                   // 2
                                                                                   //
var home = process.env.PWD.replace('.meteor/local/cordova-build', ''),             // 4
    fs = require('fs'),                                                            // 4
    exec = require('child_process'),                                               // 4
    path = home + 'server/pictures/',                                              // 4
    etnbr = path.replace('pictures/', 'ETNBr/etnbr ');                             // 4
                                                                                   //
Meteor.startup(function () {                                                       // 12
  Meteor.methods({                                                                 // 13
    process: function process(data) {                                              // 14
      var img = 'data:image/jpg;base64,' + data,                                   // 15
          buf = new Buffer(img.replace(/^data:image\/\w+;base64,/, ""), 'base64');
      var pic = '' + path + SHA256(data) + '.jpg';                                 // 17
                                                                                   //
      fs.writeFileSync(pic, buf);                                                  // 19
      delete buf, img;                                                             // 20
                                                                                   //
      var result = exec.execSync(etnbr + pic).toString().split("\n");              // 22
      exec.exec("rm " + pic);                                                      // 23
                                                                                   //
      console.log(SHA256(data));                                                   // 25
                                                                                   //
      Meteor.publish(SHA256(data), function () {                                   // 27
        return Person.find({ caminhoAbsoluto: { $in: result } });                  // 28
      });                                                                          // 29
    },                                                                             // 30
    stopPublish: function stopPublish(hash) {                                      // 31
      Meteor.publish(hash, _this.stop());                                          // 32
    }                                                                              // 33
  });                                                                              // 13
});                                                                                // 35
/////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json"]});
require("./lib/router.js");
require("./collections/person.js");
require("./server/main.js");
//# sourceMappingURL=app.js.map
